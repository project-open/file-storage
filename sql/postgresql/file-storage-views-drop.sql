--
-- file-storage/sql/postresql/file-storage-views-drop.sql
--
-- @author yon (yon@openforce.net)
-- @creation-date 2002-04-03
-- @version $Id: file-storage-views-drop.sql,v 1.3 2015/12/04 14:02:55 cvs Exp $
--

drop view if exists fs_objects;
drop view if exists fs_files;
drop view if exists fs_folders;
drop view if exists fs_urls_full;
